let classBaseUrl = `${baseUrl}/classes`;

let classAPI = {
    getAll: function ({
        currentPage,
        limit,
        currentSearch,
        currentFieldSort,
        isSortASC,
        currentStatusFilter,
        success }) {

        let url = `${classBaseUrl}?page=${currentPage}&size=${limit}`;

        // sort
        if (currentFieldSort) {
            url += `&sort=${currentFieldSort},${isSortASC ? "ASC" : "DESC"}`;
        }

        // search
        if (currentSearch) {
            url += `&search=${currentSearch}`;
        }

        // filter
        if (currentStatusFilter) {
            url += `&status=${currentStatusFilter}`;
        }

        $.ajax({
            url: url,
            type: 'GET',
            contentType: "application/json",
            dataType: 'json',
            success: success,
            error(jqXHR, _, errorThrown) {
                handleCommonAPIError(
                    "Get List User",
                    jqXHR, errorThrown,
                    () => userAPI.getAll({
                        currentPage,
                        limit,
                        currentSearch,
                        currentFieldSort,
                        isSortASC,
                        currentFieldSort,
                        currentStatusFilter,
                        success
                    }));
            }
        });
    }
}