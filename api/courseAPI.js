let courseBaseUrl = `${baseUrl}/courses`;

let courseAPI = {
    getAll: function ({
        currentPage,
        limit,
        currentSearch,
        currentFieldSort,
        isSortASC,
        success }) {

        let url = `${courseBaseUrl}?page=${currentPage}&size=${limit}`;
        // search
        if (currentSearch) {
            url += `&search=${currentSearch}`;
        }
        // sort
        if (currentFieldSort) {
            url += `&sort=${currentFieldSort},${isSortASC ? "asc" : "desc"}`;
        }

        $.ajax({
            url: url,
            type: 'GET',
            contentType: "application/json",
            dataType: 'json',
            success: success,
            error(jqXHR, _, errorThrown) {
                handleCommonAPIError(
                    "Get List Course",
                    jqXHR, errorThrown,
                    () => courseAPI.getAll({
                        currentPage,
                        limit,
                        currentSearch,
                        currentFieldSort,
                        isSortASC,
                        success
                    }));
            }
        });
    }
}