let mentorBaseUrl = `${baseUrl}/mentors`;

let mentorAPI = {
    getAll: function ({
        currentPage,
        limit,
        currentSearch,
        currentStatusFilter,
        currentFieldSort,
        isSortASC,
        success }) {

        let url = `${mentorBaseUrl}?page=${currentPage}&size=${limit}`;

        // sort
        if (currentFieldSort) {
            url += `&sort=${currentFieldSort},${isSortASC ? "ASC" : "DESC"}`;
        }

        // filter
        if (currentStatusFilter) {
            url += `&status=${currentStatusFilter}`;
        }

        // search
        if (currentSearch) {
            url += `&search=${currentSearch}`;
        }
        

        $.ajax({
            url: url,
            type: 'GET',
            contentType: "application/json",
            dataType: 'json',
            success: success,
            error(jqXHR, _, errorThrown) {
                handleCommonAPIError(
                    "Get List Mentor",
                    jqXHR, errorThrown,
                    () => mentorAPI.getAll({
                        currentPage,
                        limit,
                        currentSearch,
                        currentStatusFilter,
                        currentFieldSort,
                        isSortASC,
                        success
                    }));
            }
        });
    }
}