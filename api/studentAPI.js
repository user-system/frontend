let studentBaseUrl = `${baseUrl}/students`;

let studentAPI = {
    getAll: function ({
        currentPage,
        limit,
        currentSearch,
        currentStatusFilter,
        currentFieldSort,
        isSortASC,
        success }) {

        let url = `${studentBaseUrl}?page=${currentPage}&size=${limit}`;

        // sort
        if (currentFieldSort) {
            url += `&sort=${currentFieldSort},${isSortASC ? "ASC" : "DESC"}`;
        }

        // filter
        if (currentStatusFilter) {
            url += `&status=${currentStatusFilter}`;
        }

        // search
        if (currentSearch) {
            url += `&search=${currentSearch}`;
        }
        

        $.ajax({
            url: url,
            type: 'GET',
            contentType: "application/json",
            dataType: 'json',
            success: success,
            error(jqXHR, _, errorThrown) {
                handleCommonAPIError(
                    "Get List Student",
                    jqXHR, errorThrown,
                    () => studentAPI.getAll({
                        currentPage,
                        limit,
                        currentSearch,
                        currentStatusFilter,
                        currentFieldSort,
                        isSortASC,
                        success
                    }));
            }
        });
    }
}
