let subjectBaseUrl = `${baseUrl}/subjects`;

let subjectAPI = {
    getAll: function ({
        currentPage,
        limit,
        currentSearch,
        currentFieldSort,
        isSortASC,
        success }) {
        let url = `${subjectBaseUrl}?page=${currentPage}&size=${limit}`;
        
        if (currentFieldSort) {
            url += `&sort=${currentFieldSort},${isSortASC ? "asc" : "desc"}`;
        }
        if (currentSearch) {
            url += `&search=${currentSearch}`;
        }
        $.ajax({
            url: url,
            type: 'GET',
            contentType: "application/json",
            dataType: 'json',
            success: success,
            error(jqXHR, _, errorThrown) {
                handleCommonAPIError(
                    "Get List Subjects",
                    jqXHR, errorThrown,
                    () => subjectAPI.getAll({
                        currentPage,
                        limit,
                        currentSearch,
                        currentFieldSort,
                        isSortASC,
                        success
                    }));
            }
        });
    }
}