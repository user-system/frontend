let addSubjectFormValidator;
let updateSubjectFormValidator;
let addSubjectMilestoneFormValidator;
let updateSubjectMilestoneFormValidator;
let confirmOtpFormValidator;
let resetPasswordFormValidator;
let registerFormValidator;
let loginFormValidator;
let changePasswordFormValidator;
let addSettingFormValidator;
let updateSettingFormValidator;
let addUserFormValidator;
let updateUserFormValidator;
let updateProfileFormValidator;

function showErrorMessage(idInput, idLabel, errorMessage) {
  $(`#${idInput}`).addClass("is-invalid");
  $(`#${idLabel}`).removeClass("d-none");
  if (errorMessage) {
    $(`#${idLabel}`).text(errorMessage);
  }
}

function hideErrorMessage(idInput, idLabel) {
  $(`#${idInput}`).removeClass("is-invalid");
  $(`#${idLabel}`).addClass("d-none");
}

function initBaseValidator({ formValidator, rules, messages, submitHandler }) {
  $(`${formValidator}`).validate().destroy();
  $(`${formValidator}`).validate({
    rules: rules,
    messages: messages,
    submitHandler: function (form) {
      submitHandler();
      return false;
    },
    // Errors
    errorPlacement: function errorPlacement(error, element) {
      var $parent = $(element).parents(".form-group");
      // Do not duplicate errors
      if ($parent.find(".jquery-validation-error").length) {
        return;
      }
      $parent.append(
        error.addClass(
          "jquery-validation-error small form-text invalid-feedback"
        )
      );
    },
    highlight: function (element) {
      var $el = $(element);
      var $parent = $el.parents(".form-group");
      $el.addClass("is-invalid");
      // Select2 and Tagsinput
      if (
        $el.hasClass("select2-hidden-accessible") ||
        $el.attr("data-role") === "tagsinput"
      ) {
        $el.parent().addClass("is-invalid");
      }
    },
    unhighlight: function (element) {
      $(element)
        .parents(".form-group")
        .find(".is-invalid")
        .removeClass("is-invalid");
    },
  });
}

function resetValidator(formValidator) {
  if (formValidator) {
    try {
      formValidator.resetForm();
    } catch (e) { }
    $("input").removeClass("is-invalid");
  }
}

function initAddSettingFormValidator({ submitHandler }) {
  initBaseValidator({
    formValidator: "#setting-form",
    rules: {
      "validation-settingType": {
        required: true,
      },
      "validation-settingName": {
        required: true,
        maxlength: 50,
        remote: {
          url: `${settingBaseUrl}/name/exists`,
          type: "GET",
          data: {
            name: function () {
              return $("#settingName-input").val();
            },
          },
          dataFilter: function (data) {
            return !(data == "true");
          },
        },
      },
    },
    messages: {
      "validation-settingName": {
        maxlength: "The max length of name is 50 characters",
        remote: "The setting's name already exists",
      },
    },
    submitHandler,
  });
  addSettingFormValidator = $("#setting-form");
}

function initLoginFormValidator({ submitHandler }) {
  initBaseValidator({
    formValidator: "#login-form",
    rules: {
      "validation-email": {
        required: true
      },
      "validation-password": {
        required: true
      }
    },
    submitHandler
  });
  loginFormValidator = $("#login-form");
}

function initChangePasswordFormValidator({ submitHandler }) {
  initBaseValidator({
    formValidator: "#change-password-form",
    rules: {
      "validation-old-password": {
        required: true,
        remote: {
          url: `${baseUrl}/auth/old-password-match`,
          type: "GET",
          headers: {
            "Authorization": `Bearer ${storage.getToken()}`
          },
          data: {
            oldPassword: function () {
              return $("#old-password-input").val();
            },
          },
          dataFilter: function (data) {
            return data == "true";
          }
        }
      },
      "validation-new-password": {
        required: true
      },
      "validation-confirm-password": {
        required: true,
        equalTo: "#new-password-input"
      }
    },
    messages: {
      "validation-old-password": {
        remote: "The old password is incorrect"
      },
      "validation-confirm-password": {
        equalTo: "Password & Confirm Password are not matching"
      }
    },
    submitHandler
  });
  changePasswordFormValidator = $("#change-password-form");
}

function initRegisterFormValidator({ submitHandler }) {
  initBaseValidator({
    formValidator: "#register-form",
    rules: {
      "validation-fullName": {
        required: true
      },
      "validation-email": {
        required: true,
        remote: {
          url: `${baseUrl}/users/email/exists`,
          type: "GET",
          data: {
            email: function () {
              return $("#email-input").val();
            },
          },
          dataFilter: function (data) {
            return !(data == "true");
          }
        }
      },
      "validation-password": {
        required: true
      },
      "validation-confirmPassword": {
        required: true,
        equalTo: "#password-input"
      }
    },
    messages: {
      "validation-email": {
        remote: jQuery.validator.format("{0} is already in use")
      },
      "validation-confirmPassword": {
        equalTo: "Password & Confirm Password are not matching"
      }
    },
    submitHandler
  });
  registerFormValidator = $("#register-form");
}

function initResetPasswordFormValidator({ submitHandler }) {
  initBaseValidator({
    formValidator: "#reset-password-form",
    rules: {
      "validation-email": {
        required: true
      }
    },
    submitHandler
  });
  resetPasswordFormValidator = $("#reset-password-form");
}

function initConfirmOtpFormValidator({ submitHandler }) {
  initBaseValidator({
    formValidator: "#confirm-otp-form",
    rules: {
      "validation-otp": {
        required: true
      }
    },
    submitHandler
  });
  confirmOtpFormValidator = $("#confirm-otp-form");
}

function resetAddSettingValidator() {
  resetValidator(addSettingFormValidator);
}

function resetChangePasswordFormValidator() {
  resetValidator(changePasswordFormValidator);
}

function resetAddUserValidator() {
  resetValidator(addUserFormValidator);
}

//Add new user
function initAddUserFormValidator({ submitHandler }) {
  initBaseValidator({
    formValidator: "#user-form",
    rules: {
      "validation-fullName": {
        required: true,
      },
      "validation-email": {
        required: true,
        email: true,
        remote: {
          url: `${userBaseUrl}/email/exists`,
          type: "GET",
          data: {
            email: function () {
              return $("#email-input").val();
            },
          },
          dataFilter: function (data) {
            return !(data == "true");
          },
        },
      },
      "validation-mobile": {
        required: true,
        pattern: "[0-9]+",
        minlength: 10,
        remote: {
          url: `${userBaseUrl}/mobile/exists`,
          type: "GET",
          data: {
            mobile: function () {
              return $("#mobile-input").val();
            },
          },
          dataFilter: function (data) {
            return !(data == "true");
          },
        },
      },
      "validation-role": {
        required: true,
      },
    },
    messages: {
      "validation-email": {
        remote: "The user's email already exists",
      },
      "validation-mobile": {
        remote: "The user's mobile already exists",
      },
    },
    submitHandler,
  });
  addUserFormValidator = $("#user-form");
}


function resetAddSubjectValidator() {
  resetValidator(addSubjectFormValidator);
}

function initAddSubjectFormValidator({ submitHandler }) {
  initBaseValidator({
    formValidator: "#subject-form",
    rules: {
      "validation-subject-name": {
        required: true,
        remote: {
          url: `${subjectBaseUrl}/name/exists`,
          type: "GET",
          data: {
            name: function () {
              return $("#subject-name-input").val();
            },
          },
          dataFilter: function (data) {
            return !(data == "true");
          },
        },
      },
      "validation-faculty": {
        required: true
      },
      "validation-duration": {
        required: true,
        min: 1
      },
      "validation-manager": {
        required: true
      },
    },
    messages: {
      "validation-subject-name": {
        remote: "The subject's name already exists",
      },
    },
    submitHandler
  });
  addSubjectFormValidator = $("#subject-form");
}

function initUpdateSubjectFormValidator({ submitHandler }) {
  initBaseValidator({
    formValidator: "#subject-form",
    rules: {
      "validation-subject-name": {
        required: true,
        remote: {
          url: `${subjectBaseUrl}/name/exists`,
          type: "GET",
          data: {
            name: function () {
              return $("#subject-name-input").val();
            },
          },
          dataFilter: function (data) {
            if ($("#subject-name-input").val() == oldSettingNameForUpdate) {
              return true;
            }
            return !(data == "true");
          },
        },
      },
      "validation-duration": {
        required: true,
        min: 1,
      },
      "validation-faculty": {
        required: true,
      },
      "validation-manager": {
        required: true,
      }
    },
    messages: {
      "validation-subject-name": {
        remote: "The subject's name already exists",
      },
    },
    submitHandler,
  });
  updateSubjectFormValidator = $("#subject-form");
}

function resetUpdateSubjectValidator() {
  resetValidator(updateSubjectFormValidator);
}

function initUpdateProfileFormValidator({ submitHandler }) {
  initBaseValidator({
    formValidator: "#update-profile-form",
    rules: {
      "validation-fullName": {
        required: true,
      },
      "validation-faculty": {
        required: true,
      },
      "validation-gender": {
        required: true,
      },
      "validation-roleId": {
        required: true,
      },
      "validation-mobile": {
        required: true,
      }
    },
    submitHandler,
  });
  updateProfileFormValidator = $("#update-profile-form");
}

function resetUpdateUserValidator() {
  resetValidator(updateUserFormValidator);
}

function initAddSubjectMilestoneFormValidator({ submitHandler }) {
  initBaseValidator({
    formValidator: "#subject-milestone-form",
    rules: {
      "validation-title": {
        required: true,
      },
      "validation-step": {
        required: true,
        min: 1,
      },
      "validation-duration": {
        required: true,
        min: 1,
      },
    },
    submitHandler
  });
  addSubjectMilestoneFormValidator = $("#subject-milestone-form");
}

function resetAddSubjectMilestoneValidator() {
  resetValidator(addSubjectMilestoneFormValidator);
}

function initUpdateSubjectMilestoneFormValidator({ submitHandler }) {
  initBaseValidator({
    formValidator: "#subject-milestone-form",
    rules: {
      "validation-title": {
        required: true,
      },
      "validation-step": {
        required: true,
        min: 1,
      },
      "validation-duration": {
        required: true,
        min: 1,
      },
    },
    submitHandler,
  });
  updateSubjectMilestoneFormValidator = $("#subject-milestone-form");
}

function resetUpdateSubjectMilestoneValidator() {
  resetValidator(updateSubjectMilestoneFormValidator);
}

//Update setting
function initUpdateSettingFormValidator({ submitHandler }) {
  initBaseValidator({
    formValidator: "#setting-form",
    rules: {
      "validation-settingType": {
        required: true,
      },
      "validation-settingName": {
        required: true,
        remote: {
          url: `${settingBaseUrl}/name/exists`,
          type: "GET",
          data: {
            name: function () {
              return $("#settingName-input").val();
            },
          },
          dataFilter: function (data) {
            if ($("#settingName-input").val() == oldSettingNameForUpdate) {
              return true;
            }
            return !(data == "true");
          },
        },
      },
    },
    messages: {
      "validation-settingName": {
        remote: "The setting's name already exists",
      },
    },
    submitHandler,
  });
  updateSettingFormValidator = $("#setting-form");
}

function resetUpdateSettingValidator() {
  resetValidator(updateSettingFormValidator);
}

function initUpdateUserFormValidator({ submitHandler }) {
  initBaseValidator({
    formValidator: "#user-form",
    rules: {
      "validation-role": {
        required: true,
      },
      "validation-status": {
        required: true,
      },
    },
    submitHandler,
  });
  updateUserFormValidator = $("#user-form");
}

function initUpdateProfileFormValidator({ submitHandler }) {
  initBaseValidator({
    formValidator: "#update-profile-form",
    rules: {
      "validation-name": {
        required: true,
      },
      "validation-faculty": {
        required: true,
      },
      "validation-gender": {
        required: true,
      },
      "validation-mobile": {
        required: true,
      }
    },
    submitHandler,
  });
  updateProfileFormValidator = $("#update-profile-form");
}

function resetUpdateProfileValidator() {
  resetValidator(updateProfileFormValidator);
}

function resetUpdateUserValidator() {
  resetValidator(updateUserFormValidator);
}
