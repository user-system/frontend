let baseUrl = "http://localhost:8080/api/v1";
let currentPage = 1;
let limit = 10;
let currentSearch = "";
let currentFieldSort = "id";
let isSortASC = false;
let currentStatusFilter = "";

// view list
function fillPaginationToTable(totalItem, changePageFunction, idPagination) {
    let totalPages = Math.ceil(totalItem / limit);

    let rows = "";
    // previous
    rows += `<li class="page-item ${currentPage == 1 ? "disabled" : ""}" onclick="${changePageFunction}(${currentPage - 1}, ${totalPages})">
                <a class="page-link" href="#">Previous</a>
            </li>`;

    for (let i = 1; i <= totalPages; i++) {
        let row = `<li class="page-item ${currentPage == i ? "active" : ""}" onclick="${changePageFunction}(${i}, ${totalPages})">
                        <a class="page-link" href="#">${i}</a>
                    </li>`;
        rows += row;
    }

    rows += `<li class="page-item ${currentPage == totalPages ? "disabled" : ""}" onclick="${changePageFunction}(${currentPage + 1}, ${totalPages})">
                <a class="page-link" href="#">Next</a>
            </li>`;

    $(`${idPagination}`).empty();
    $(`${idPagination}`).append(rows);
}

function changePage({ newPage, totalPages, getListFunction }) {
    if (newPage == currentPage || newPage < 1 || newPage > totalPages) {
        return;
    }
    currentPage = newPage;

    getListFunction();
};

function resetPaging() {
    currentPage = 1;
}

function changeSearch({ idSearchInput, getListFunction }) {
    let inputSearch = $(idSearchInput).val();
    if (inputSearch != currentSearch) {
        currentSearch = inputSearch;
        resetPaging();
        getListFunction();
    }
};

function resetSearch(idSearchInput) {
    currentSearch = "";
    $(idSearchInput).val("");
}

function SortObject(name, iconSortId) {
    this.name = name;
    this.iconSortId = iconSortId;
}

function changeSort({ field, getListFunction, sortObjects }) {
    if (field == currentFieldSort) {
        isSortASC = !isSortASC;
    } else {
        currentFieldSort = field;
        isSortASC = true;
    }
    sortObjects.forEach(item => {
        if (currentFieldSort == item.name) {
            showSortIcon(item.iconSortId, sortObjects.map(item => item.iconSortId));
        }
    });
    resetPaging();
    getListFunction();
}

function showSortIcon(sortIconId, allSortIcons) {
    // hideAllSortIcon
    allSortIcons.forEach(item => {
        hideSortIcon(item);
    });
    // show icon
    $(`#${sortIconId}`).removeClass("d-none");
    //set icon
    $(`#${sortIconId}`).empty();
    $(`#${sortIconId}`).prepend(`<i data-feather="${isSortASC ? "chevron-up" : "chevron-down"}"></i>`)
    feather.replace();
};

function hideSortIcon(sortIconId) {
    $(`#${sortIconId}`).addClass("d-none");
}

function resetSort() {
    currentFieldSort = "";
    isSortASC = "";
}

function handleCommonAPIError(functionName, jqXHR, errorThrown, continueingAPI) {
    // internal server
    if (jqXHR.status == 500) {
        // location.href = '../common/page-500.html';
        return;
    }

    // error
    showNotification(functionName, "Fail! There is a error!", false);
    console.log(jqXHR);
    console.log(errorThrown);
}

$('[data-toggle="tooltip"]').tooltip();
