function loadClassPages() {
    $("#content").load("../pages/class/class.html", function () {
        feather.replace();
        settingCommonForClassPage();
        refreshTableClass();
    });
}

function settingCommonForClassPage() {
    // common settings 
    $('[data-toggle="tooltip"]').tooltip();

}