function getClassDataForTable() {
    classAPI.getAll({
        currentPage,
        limit,
        currentSearch,
        isSortASC,
        currentFieldSort,
        currentStatusFilter,
        success: function (data) {
            fillListClassToTable(data.content);
            fillPaginationToTable(data.totalElements, "changeClassPage", "#class-pagination");
        }
    });
}

function fillListClassToTable(classes) {
    let rows = "";

    for (const clazz of classes) {
        let row = ` <tr>                    
                            <td>${clazz.name || " "}</td>
                            <td>${displayClassViewList(clazz.schedules, "schedules")}</td>
                            <td>${clazz.address || " "}</td>
                            <td>${displayClassViewList(clazz.currentLessonSubjectMentor, "LessonSubject")}</td>
                            <td>${displayClassViewList(clazz.currentLessonSubjectMentor, "mentor")}</td>
                            <td>${clazz.studentRate.dropoutRate}%</td>
                            <td>${clazz.studentRate.deferredRate}%</td>
                            <td>${clazz.status || " "}</td>
                    </tr>`;
        rows += row;
    }
    $('#class-table-body').empty();
    $('#class-table-body').append(rows);
    $('#filtter-status').select2();
}

function displayClassViewList(field, item) {
    if (!field) {
        return "";
    }

    let lesson = field.lessonName || "";
    let subject = field.subjectName || "";
    let mentor = field.mentorFullname || "";

    if (item == "mentor") {
        return `${mentor}`;
    }
    if (item == "schedules") {
        let scheduleNames = field.map(schedule => schedule.name).join(', ');
        return scheduleNames;
    }

    return `${lesson} - ${subject}`;
}

// paging
function changeClassPage(newPage, totalPages) {
    changePage({
        newPage,
        totalPages,
        getListFunction: getClassDataForTable
    });
}

// search
function changeClassSearch() {
    changeSearch({
        idSearchInput: "#class-search",
        getListFunction: getClassDataForTable
    });
}

function resetClassSearch() {
    resetSearch("#class-search");
}

// sort
function changeClassSort(field) {
    changeSort({
        field,
        getListFunction: getClassDataForTable,
        sortObjects: new Array(
            new SortObject('Name', 'class-name-sort-icon'),
            new SortObject('Schedule', 'class-schedule-sort-icon'),
            new SortObject('Address', 'class-address-sort-icon'),
            new SortObject('currentLessonSubjectMentor.lessonName', 'class-lesson-sort-icon'),
            new SortObject('currentLessonSubjectMentor.mentorFullname', 'class-mentor-sort-icon'),
            new SortObject('studentRate.dropoutRate', 'class-DORate-sort-icon'),
            new SortObject('studentRate.deferredRate', 'class-DRate-sort-icon'),
            new SortObject('Status', 'class-status-sort-icon')
        )
    })
}

// refresh
function refreshTableClass() {
    resetPaging();
    resetClassSearch();
    resetSort();
    resetFilterClass();
    hideAllSortIconClass();
    getClassDataForTable();
}

// filter
function changeClassFilter() {
    let valueFilterClass = $('#filtter-status').val();
     if (valueFilterClass != currentStatusFilter) {
        currentStatusFilter = valueFilterClass;
        resetPaging();
        getClassDataForTable();
    }
}

function resetFilterClass() {
    currentStatusFilter = "";
    $('#filtter-status').val("");
}

function hideAllSortIconClass() {
    hideSortIcon('class-name-sort-icon');
    hideSortIcon('class-schedule-sort-icon');
    hideSortIcon('class-address-sort-icon');
    hideSortIcon('class-lesson-sort-icon');
    hideSortIcon('class-mentor-sort-icon');
    hideSortIcon('class-DORate-sort-icon');
    hideSortIcon('class-DRate-sort-icon');
    hideSortIcon('class-status-sort-icon');
}