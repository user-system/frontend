function loadCoursePages() {
    $("#content").load("../pages/course/course.html", function () {
        feather.replace();
        settingCommonForCoursePage();
        refreshTableCourse();
    });
}
function settingCommonForCoursePage() {
    $('[data-toggle="tooltip"]').tooltip();
}