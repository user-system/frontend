
function getCourseDataForTable() {
    courseAPI.getAll({
        currentPage,
        limit,
        currentSearch,
        currentFieldSort,
        isSortASC,
        success: function (data) {
            fillListCourseToTable(data.content);
            fillPaginationToTable(data.totalElements, "changeCoursePage", "#course-pagination");
        }
    });
}

function fillListCourseToTable(courses) {
    let rows = "";

    for (const course of courses) {
        let row = ` <tr>
                        <td>${course.name}</td>
                        <td>${course.TotalSubject}</td>
                        <td>${course.TotalLesson}</td>
                        <td>${course.creatorFullName}</td>
                        <td>${course.createdDate}</td>
                        <td>${course.standardTuition}</td>
                    </tr>`;
        rows += row;
    }
    $('#course-table-body').empty();
    $('#course-table-body').append(rows);
}

function changeCoursePage(newPage, totalPages) {
    changePage({
        newPage,
        totalPages,
        getListFunction: getCourseDataForTable
    });
};

function changeSearchCourse() {
    changeSearch({
        idSearchInput: "#course-search",
        getListFunction: getCourseDataForTable
    });
};

function resetCourseSearch() {
    resetSearch("#course-search");
}

function changeSortCourse(field) {
    changeSort({
        field,
        getListFunction: getCourseDataForTable,
        sortObjects: new Array(
            new SortObject('name', 'course-name-sort-icon'),
            new SortObject('totalSubjectTotalLessonTotalSubject', 'course-totalSubject-sort-icon'),
            new SortObject('totalSubjectTotalLessonTotalLesson', 'course-totalLesson-sort-icon'),
            new SortObject('creator', 'course-creator-sort-icon'),
            new SortObject('createdDate', 'course-createdDate-sort-icon'),
            new SortObject('standardTuition', 'course-tuiton-sort-icon')
        )
    })
}

function refreshTableCourse() {
    resetPaging();
    resetCourseSearch();
    resetSort();
    hideAllSortIconCourse();
    getCourseDataForTable();
}

function hideAllSortIconCourse(){

    hideSortIcon('course-name-sort-icon');
    hideSortIcon('course-totalSubject-sort-icon');
    hideSortIcon('course-totalLesson-sort-icon');
    hideSortIcon('course-creator-sort-icon');
    hideSortIcon('course-createdDate-sort-icon');
    hideSortIcon('course-tuiton-sort-icon');
};