function loadMentorPages() {
    $("#content").load("../pages/mentor/mentor.html", function () {
        feather.replace();
        settingCommonForMentorPage()
        refreshTableMentor();
    });
}
function settingCommonForMentorPage() {
    $('[data-toggle="tooltip"]').tooltip();

}