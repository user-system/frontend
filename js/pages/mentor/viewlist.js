function getMentorDataForTable() {
    mentorAPI.getAll({
        currentPage,
        limit,
        currentSearch,
        currentStatusFilter,
        currentFieldSort,
        isSortASC,
        success: function (data) {
            fillListMentorToTable(data.content);
            fillPaginationToTable(data.totalElements, "changeMentorPage", "#mentor-pagination");
        }
    });
}

function fillListMentorToTable(mentors) {
    let rows = "";

    for (const mentor of mentors) {
        let row = ` <tr>
                        <td>${mentor.fullname}</td>
                        <td>${mentor.totalHourInThisMonth || "0"}</td>
                        <td>${mentor.teachings}</td>
                        <td>${mentor.creatorFullname}</td>
                        <td>${mentor.createdDate}</td>
                        <td>${mentor.status}</td>

                    </tr>`;
        rows += row;
    }
    $('#mentor-table-body').empty();
    $('#mentor-table-body').append(rows);
}

function changeMentorPage(newPage, totalPages) {
    changePage({
        newPage,
        totalPages,
        getListFunction: getMentorDataForTable
    });
};

function changeSearchMentor() {
    changeSearch({
        idSearchInput: "#mentor-search",
        getListFunction: getMentorDataForTable
    });
};

function resetMentorSearch() {
    resetSearch("#mentor-search");
}

function changeSortMentor(field) {
    changeSort({
        field,
        getListFunction: getMentorDataForTable,
        sortObjects: new Array(
            new SortObject('fullname', 'mentor-fullname-sort-icon'),
            new SortObject('totalHourInThisMonth', 'mentor-total-hours-sort-icon'),
            new SortObject('status', 'mentor-status-sort-icon'),
            new SortObject('creator', 'mentor-creator-sort-icon'),
            new SortObject('createdDate', 'mentor-created-date-sort-icon')
        )
    })
}

function refreshTableMentor() {
    resetPaging();
    resetMentorSearch();
    resetSort();
    resetMentorFilter();
    hideAllSortIconMentor();

    getMentorDataForTable();
}

function changeMentorFilter() {
    let valueFilterMentor = $('#status-mentor').val();
     if (valueFilterMentor != currentStatusFilter) {
        currentStatusFilter = valueFilterMentor;
        resetPaging();
        getMentorDataForTable();
    }
}

function resetMentorFilter(){
    currentStatusFilter = "";
    $('#status-mentor').prop('selectedIndex', 0);
}

function hideAllSortIconMentor() {

    hideSortIcon('mentor-fullname-sort-icon');
    hideSortIcon('mentor-total-hours-sort-icon');
    hideSortIcon('mentor-status-sort-icon');
    hideSortIcon('mentor-creator-sort-icon');
    hideSortIcon('mentor-created-date-sort-icon');
}
