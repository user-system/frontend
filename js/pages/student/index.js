function loadStudentPages() {
    $("#content").load("../pages/student/student.html", function () {
        feather.replace();
        settingCommonForStudentPage();
        refreshTableStudent();
    });
}
function settingCommonForStudentPage() {
    $('[data-toggle="tooltip"]').tooltip();

}