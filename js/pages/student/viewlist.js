function getStudentDataForTable() {
    studentAPI.getAll({
        currentPage,
        limit,
        currentSearch,
        currentStatusFilter,
        currentFieldSort,
        isSortASC,
        success: function (data) {
            fillListStudentToTable(data.content);
            fillPaginationToTable(data.totalElements, "changeStudentPage", "#student-pagination");
        }
    });
}

function fillListStudentToTable(students) {
    let rows = "";

    for (const student of students) {
        let row = ` <tr>
                        <td>${student.code || " "}</td>
                        <td>${student.username || " "}</td>
                        <td>${student.fullname || " "}</td>
                        <td>${student.phoneNumber || " "}</td>
                        <td>${student.email || " "}</td>
                        <td>${displayClass(student.classes)}</td>
                        <td>${student.status || " "}</td>

                    </tr>`;
        rows += row;
    }
    $('#student-table-body').empty();
    $('#student-table-body').append(rows);
}

function displayClass(classes) {
    if (!classes) {
        return "";
    }
    return classes.map(classes => classes.name).join(', ');
}

function changeStudentPage(newPage, totalPages) {
    changePage({
        newPage,
        totalPages,
        getListFunction: getStudentDataForTable
    });
};

function changeSearchStudent() {
    changeSearch({
        idSearchInput: "#student-search",
        getListFunction: getStudentDataForTable
    });
};

function resetStudentSearch() {
    resetSearch("#student-search");
}

function changeSortStudent(field) {
    changeSort({
        field,
        getListFunction: getStudentDataForTable,
        sortObjects: new Array(
            new SortObject('code', 'student-code-sort-icon'),
            new SortObject('username', 'student-username-sort-icon'),
            new SortObject('fullname', 'student-fullname-sort-icon'),
            new SortObject('phoneNumber', 'student-phonenumber-sort-icon'),
            new SortObject('email', 'student-email-sort-icon'),
            new SortObject('classStudent.clazzName', 'student-class-sort-icon'),
            new SortObject('status', 'student-status-sort-icon'),

        )
    })
}

function refreshTableStudent() {
    resetPaging();
    resetStudentSearch();
    resetSort();
    resetSelect();
    hideAllSortIconStudent();

    getStudentDataForTable();
}

function changeStudentFilter() {
    let valueFilterStudent = $('#status-student').val();
     if (valueFilterStudent != currentStatusFilter) {
        currentStatusFilter = valueFilterStudent;
        resetPaging();
        getStudentDataForTable();
    }
}

function resetSelect() {
    $('#status-student').prop('selectedIndex', 0);
}
function hideAllSortIconStudent() {

    hideSortIcon('student-code-sort-icon');
    hideSortIcon('student-username-sort-icon');
    hideSortIcon('student-fullname-sort-icon');
    hideSortIcon('student-phonenumber-sort-icon');
    hideSortIcon('student-email-sort-icon');
    hideSortIcon('student-class-sort-icon');
    hideSortIcon('student-status-sort-icon');
}
