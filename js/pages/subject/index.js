function loadSubjectPages() {
    $("#content").load("../pages/subject/index.html", function () {
        feather.replace();
        settingCommonForSubjectPage();
        refreshTableSubject();        
    });
}
function settingCommonForSubjectPage() {
    $('[data-toggle="tooltip"]').tooltip();

}
