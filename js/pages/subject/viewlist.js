function getSubjectDataForTable() {
  subjectAPI.getAll({
    currentPage,
    limit,
    currentSearch,
    currentFieldSort,
    isSortASC,
    success: function (data) {
      let totalItems = data.totalElements;
      fillListSubjectToTable(data.content);
      fillPaginationToTable(totalItems, "changeSubjectPage","#subject-pagination");
    },
  });
}
function fillListSubjectToTable(subjects) {
  let rows = "";
  for (const sub of subjects) {
    let row = `<tr>
                        <td>${sub.name}</td>
                        <td>${sub.totalLesson}</td>
                        <td>${sub.creatorFullName}</td>
                        <td>${sub.createdDate}</td>
                    </tr>`;
    rows += row;
  }
  $("#subject-table-body").empty();
  $("#subject-table-body").append(rows);
}
function changeSubjectPage(newPage, totalPages) {
  changePage({
      newPage,
      totalPages,
      getListFunction: getSubjectDataForTable
  });
};
function changeSearchSubject() {
  changeSearch({
      idSearchInput: "#subject-search",
      getListFunction: getSubjectDataForTable
  });
};

function resetSubjectSearch() {
  resetSearch("#subject-search");
}
function changeSortSubject(field) {
  changeSort({
      field,
      getListFunction: getSubjectDataForTable,
      sortObjects: new Array(
          new SortObject('name', 'subject-name-sort-icon'),
          new SortObject('totalLesson', 'subject-totalLesson-sort-icon'),
          new SortObject('creator', 'subject-creator-sort-icon'),
          new SortObject('createdDate', 'subject-createdDate-sort-icon')
      )
  })
}
function refreshTableSubject() {
  resetPaging();
  resetSubjectSearch();
  resetSort();
  getSubjectDataForTable();
  hideAllSortIconSubject();
}
function hideAllSortIconSubject() {
  hideSortIcon('subject-name-sort-icon');
  hideSortIcon('subject-totalLesson-sort-icon');
  hideSortIcon('subject-creator-sort-icon');
  hideSortIcon('subject-createdDate-sort-icon');
}